<?php
/** Start the engine */
require_once( get_template_directory() . '/lib/init.php' );
require_once( get_stylesheet_directory() . '/includes/breadcrumb.php');

mod_event_pages();
function mod_event_pages() {

$ptype = get_post_type();

if ($ptype = "tribe_events") {
/** Remove the post info function */
remove_action( 'genesis_before_post_content', 'genesis_post_info' );

/** Remove the post meta function */
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );

}

}


// 	if( tribe_is_month() ) { // The Main Calendar Page
// add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );
// }

/** Set Localization (do not remove) */
load_child_theme_textdomain( 'minimum', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'minimum' ) );

/** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', __( 'Minimum Theme', 'minimum' ) );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/themes/minimum' );

/** Load Google fonts */
add_action( 'wp_enqueue_scripts', 'minimum_load_google_fonts' );
function minimum_load_google_fonts() {
wp_enqueue_style(
'google-fonts',
'https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,800,700',
array(),
PARENT_THEME_VERSION
);
}

/** Sets Content Width */
$content_width = apply_filters( 'content_width', 740, 740, 1140 );

/** Add Viewport meta tag for mobile browsers */
add_action( 'genesis_meta', 'minimum_add_viewport_meta_tag' );
function minimum_add_viewport_meta_tag() {
echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
}

/** Add new image sizes */
add_image_size( 'header', 1600, 9999, TRUE );
add_image_size( 'portfolio', 330, 230, TRUE );

/** Add support for custom background */
add_theme_support( 'custom-background' );
//* Output header <img> in title area
add_filter( 'genesis_seo_title', 'add_image_to_title', 10, 3);

function add_image_to_title($title, $inside, $wrap) {
$site_url  = get_bloginfo('url');
$theme_url = get_stylesheet_directory_uri();
return sprintf( '<h1 id="title"><div class="site-title">%2$s</div><div class="site-logo"><a href="%4$s"><img src="%3$s/images/header.png" alt="Center Grove Presbyterian Church" /></a></div></h1>', $wrap, $inside, $theme_url, $site_url );
}

/** Remove Site Tag Line **/
remove_action( 'genesis_site_description', 'genesis_seo_site_description' );

/** Unregister layout settings */
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

/** Unregister secondary sidebar */
unregister_sidebar( 'sidebar-alt' );

/** Add support for structural wraps */
add_theme_support( 'genesis-structural-wraps', array(
'header',
'nav',
'subnav',
'inner',
'footer-widgets',
'footer'
) );


/** Modify post titles for The Events Calendar PRO */
/** The following section of code was commented out by Craig on 01/30/2014 */
/** after deactivating the 3 Events Calendar plugins                       */
/*
add_filter('genesis_post_title_text', 'custom_do_post_title');
function custom_do_post_title() {

$title = get_the_title();

if ( strlen( $title ) == 0 )
return;

if( tribe_is_month() && !is_tax() ) { // The Main Calendar Page
$title = 'Events Calendar';
} else if( tribe_is_month() && is_tax() ) { // Calendar Category Pages
$title = 'Events Calendar' . ' &raquo; ' . single_term_title('', false);
//	} else if( tribe_is_event() && !tribe_is_day() && !is_single() ) { // The Main Events List
//		$title = 'Events List';
} else if( tribe_is_event() && is_single() ) { // Single Events
$title = get_the_title();
} else if( tribe_is_day() ) { // Single Event Days
$title = 'Events on: ' . date('F j, Y', strtotime($wp_query->query_vars['eventDate']));
} else if( tribe_is_venue() ) { // Single Venues
$title = get_the_title();
} else {
$title = get_the_title();
}

return $title;

}
*/
/** above section of code commented out by Craig on 01/30/2014                */

/** Customize the post info function */
add_filter( 'genesis_post_info', 'post_info_filter' );
function post_info_filter( $post_info ) {
if ( !is_page() ) {
$post_info = __( 'Posted on', 'minimum' ) . ' [post_date] // [post_comments] [post_edit]';
return $post_info;
}}

/** Customize the post meta function */
add_filter( 'genesis_post_meta', 'post_meta_filter' );
function post_meta_filter( $post_meta ) {
if ( !is_page() ) {
$post_meta = '[post_categories before="' . __( 'Filed Under: ', 'minimum' ) . '"] // [post_tags before="' . __( 'Tagged: ', 'minimum' ) . '"]';
return $post_meta;
}}

/** Modify the size of the Gravatar in the author box */
add_filter( 'genesis_author_box_gravatar_size', 'minimum_author_box_gravatar_size' );
function minimum_author_box_gravatar_size( $size ) {
return '96';
}

/** Add support for 3-column footer widgets */
add_theme_support( 'genesis-footer-widgets', 3 );

/** Add custom footer if widget area is used */
add_action( 'genesis_footer', 'minimum_custom_footer', 6 );
function minimum_custom_footer() {

if ( is_active_sidebar( 'custom-footer' ) ) {

remove_action( 'genesis_footer', 'genesis_do_footer' );
echo '<div class="custom-footer">';
dynamic_sidebar( 'custom-footer' );
echo '</div><!-- end .custom-footer -->';

}
}

/** Create portfolio custom post type */
add_action( 'init', 'minimum_portfolio_post_type' );
function minimum_portfolio_post_type() {
register_post_type( 'portfolio',
array(
'labels' => array(
'name' => __( 'Portfolio', 'minimum' ),
'singular_name' => __( 'Portfolio', 'minimum' ),
),
'exclude_from_search' => true,
'has_archive' => true,
'hierarchical' => true,
'menu_icon' => get_stylesheet_directory_uri() . '/images/icons/portfolio.png',
'public' => true,
'rewrite' => array( 'slug' => 'portfolio' ),
'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'genesis-seo' ),
)
);
}

/** Change the number of portfolio items to be displayed (props Bill Erickson) */
add_action( 'pre_get_posts', 'minimum_portfolio_items' );
function minimum_portfolio_items( $query ) {

if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'portfolio' ) ) {
$query->set( 'posts_per_page', '12' );
}

}

/** Register widget areas */
genesis_register_sidebar( array(
'id'				=> 'home-featured-full-width',
'name'			=> __( 'Home Featured Full Width', 'minimum' ),
'description'	=> __( 'This is the home featured full width section.', 'minimum' ),
) );
genesis_register_sidebar( array(
'id'				=> 'home-featured-1',
'name'			=> __( 'Home Featured #1', 'minimum' ),
'description'	=> __( 'This is the home featured #1 section.', 'minimum' ),
) );
genesis_register_sidebar( array(
'id'				=> 'home-featured-2',
'name'			=> __( 'Home Featured #2', 'minimum' ),
'description'	=> __( 'This is the home featured #2 section.', 'minimum' ),
) );
genesis_register_sidebar( array(
'id'				=> 'home-featured-3',
'name'			=> __( 'Home Featured #3', 'minimum' ),
'description'	=> __( 'This is the home featured #3 section.', 'minimum' ),
) );
genesis_register_sidebar( array(
'id'				=> 'home-featured-4',
'name'			=> __( 'Home Featured #4', 'minimum' ),
'description'	=> __( 'This is the home featured #4 section.', 'minimum' ),
) );
genesis_register_sidebar( array(
'id'				=> 'custom-footer',
'name'			=> __( 'Custom Footer', 'minimum' ),
'description'	=> __( 'This is the custom footer section.', 'minimum' ),
) );
